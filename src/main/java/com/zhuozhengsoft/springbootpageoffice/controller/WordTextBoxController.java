package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/WordTextBox")
public class WordTextBoxController {

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
          
        WordDocumentWriter doc = new WordDocumentWriter();
        doc.openDataRegion("PO_company").setValue("北京幻想科技有限公司");
        doc.openDataRegion("PO_logo").setValue("[image]/images/WordTextBox/logo.gif[/image]");
        doc.openDataRegion("PO_dr1").setValue("左边的文本:xxxx");
        poCtrl.setWriter(doc);

        //打开Word文档
        poCtrl.webOpen("/doc/WordTextBox/test.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("WordTextBox/Word");
        return mv;
    }

}
