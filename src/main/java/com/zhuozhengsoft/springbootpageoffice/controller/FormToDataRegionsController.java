package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/FormToDataRegions")
public class FormToDataRegionsController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WordDocumentWriter doc = new WordDocumentWriter();
        doc.openDataRegion("PO_Buyer").setEditing(true);
        doc.openDataRegion("PO_Company").setEditing(true);
        doc.openDataRegion("PO_Supplier").setEditing(true);
        doc.openDataRegion("PO_code").setEditing(true);
        doc.openDataRegion("PO_rmb").setEditing(true);
        doc.openDataRegion("PO_table").setEditing(true);
        poCtrl.setWriter(doc);

        //打开Word文档
        poCtrl.webOpen("/doc/FormToDataRegions/test.docx", OpenModeType.docSubmitForm, "张佚名");
        request.setAttribute("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("FormToDataRegions/Word");
        return mv;
    }

    @RequestMapping("/save")
    public void save(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "FormToDataRegions/" + fs.getFileName());
        fs.close();
    }


}
