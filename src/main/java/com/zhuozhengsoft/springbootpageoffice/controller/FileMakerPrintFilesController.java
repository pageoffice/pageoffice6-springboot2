package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.*;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping(value = "/FileMakerPrintFiles")
public class FileMakerPrintFilesController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/Default")
    public ModelAndView index(HttpServletRequest request) {

        ModelAndView mv = new ModelAndView("FileMakerPrintFiles/Default");
        return mv;
    }


    @RequestMapping(value = "/FileMaker")
    public void showWord(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        String fileName = "doc_" + id + ".doc";
        FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
        fmCtrl.fillDocument("/doc/FileMakerPrintFiles/" + fileName, DocumentOpenType.Word);
        response.getOutputStream().print(fmCtrl.getHtml());
    }

    @RequestMapping(value = "/Preview")
    public ModelAndView preview(HttpServletRequest request, Map<String, Object> map) {
        String id = request.getParameter("id");
        String fileName = "doc_" + id + ".doc";

        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        poCtrl.webOpen("/doc/FileMakerPrintFiles/" + fileName, OpenModeType.docReadOnly, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("FileMakerPrintFiles/Preview");
        return  mv;
    }
}
