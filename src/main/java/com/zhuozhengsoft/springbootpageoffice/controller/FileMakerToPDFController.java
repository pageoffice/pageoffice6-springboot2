package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.DocumentOpenType;
import com.zhuozhengsoft.pageoffice.FileMakerCtrl;
import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping(value = "/FileMakerToPDF")
public class FileMakerToPDFController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/Default")
    public ModelAndView index(HttpServletRequest request) {

        ModelAndView mv = new ModelAndView("FileMakerToPDF/Default");
        return mv;
    }


    @RequestMapping(value = "/FileMakerToPDF")
    public void showWord(HttpServletRequest request,HttpServletResponse response) throws IOException {
        FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
        fmCtrl.fillDocumentAsPDF("/doc/FileMakerToPDF/template.doc", DocumentOpenType.Word, "zhengshu.pdf");
        response.getOutputStream().print(fmCtrl.getHtml());
    }

    @RequestMapping(value = "/save")
    public void save(HttpServletRequest request,HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "FileMakerToPDF/"+ fs.getFileName());
        //设置自定义保存结果，用来返回给前端页面，setCustomSaveResult的参数也可以是json字符串类型
        //设置文件的保存结果
        fs.setCustomSaveResult("ok");
        fs.close();
    }
}
