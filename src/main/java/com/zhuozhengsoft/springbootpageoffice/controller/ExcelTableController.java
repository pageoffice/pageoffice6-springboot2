package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.ExcelTableWriter;
import com.zhuozhengsoft.pageoffice.excel.SheetWriter;
import com.zhuozhengsoft.pageoffice.excel.WorkbookWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import java.util.Map;

@RestController
@RequestMapping(value = "/ExcelTable")
public class ExcelTableController {

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //定义WorkbookWriter对象
        WorkbookWriter workBook = new WorkbookWriter();
        //定义SheetWriter对象，"Sheet1"是打开的Excel表单的名称
        SheetWriter sheet = workBook.openSheet("Sheet1");
        //定义ExcelTableWriter对象
        ExcelTableWriter table = sheet.openTable("B4:F13");
        for (int i = 0; i < 50; i++) {
            table.getDataFields().get(0).setValue("产品 " + i);
            table.getDataFields().get(1).setValue("100");
            table.getDataFields().get(2).setValue(String.valueOf(100 + i));
            table.nextRow();
        }
        table.close();
        poCtrl.setWriter(workBook);

        //打开excel文档
        poCtrl.webOpen("/doc/ExcelTable/test.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("ExcelTable/Excel");
        return mv;
    }


}
