package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.DataRegionWriter;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/WordResImage")
public class WordResImageController {
    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WordDocumentWriter worddoc = new WordDocumentWriter();
        //先在要插入word文件的位置手动插入书签,书签必须以“PO_”为前缀
        //给DataRegion赋值,值的形式为："[word]word文件路径[/word]、[excel]excel文件路径[/excel]、[image]图片路径[/image]"
        DataRegionWriter data1 = worddoc.openDataRegion("PO_p1");
        data1.setValue("[image]/doc/WordResImage/1.jpg[/image]");
        DataRegionWriter data2 = worddoc.openDataRegion("PO_p2");
        data2.setValue("[word]/doc/WordResImage/2.doc[/word]");
        DataRegionWriter data3 = worddoc.openDataRegion("PO_p3");
        data3.setValue("[word]/doc/WordResImage/3.doc[/word]");

        poCtrl.setWriter(worddoc);

        //打开Word文档
        poCtrl.webOpen("/doc/WordResImage/test.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("WordResImage/Word");
        return mv;
    }


}
