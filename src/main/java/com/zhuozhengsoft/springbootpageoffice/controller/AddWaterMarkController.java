package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/AddWaterMark")
public class AddWaterMarkController {
    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WordDocumentWriter doc = new WordDocumentWriter();
        //添加水印 ，设置水印的内容
        doc.getWaterMark().setText("PageOffice开发平台");
        poCtrl.setWriter(doc);
        //打开Word文档
        poCtrl.webOpen("/doc/AddWaterMark/test.doc", OpenModeType.docNormalEdit, "张三");
        request.setAttribute("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("AddWaterMark/Word");
        return mv;
    }


}
