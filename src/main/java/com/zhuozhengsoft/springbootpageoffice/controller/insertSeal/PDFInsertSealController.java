package com.zhuozhengsoft.springbootpageoffice.controller.insertSeal;

import com.zhuozhengsoft.pageoffice.*;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.Map;

@RestController
@RequestMapping(value = "/InsertSeal/PDF")
public class PDFInsertSealController {
    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/AddSeal/PDF1")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl1 = new PDFCtrl(request);

        pdfCtrl1.webOpen("/doc/InsertSeal/PDF/AddSeal/test1.pdf");
        map.put("pageoffice", pdfCtrl1.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/PDF/AddSeal/PDF1");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/PDF2")
    public ModelAndView showWord2(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl1 = new PDFCtrl(request);

        pdfCtrl1.webOpen("/doc/InsertSeal/PDF/AddSeal/test2.pdf");
        map.put("pageoffice", pdfCtrl1.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/PDF/AddSeal/PDF2");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/PDF3")
    public ModelAndView showWord3(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl1 = new PDFCtrl(request);

        pdfCtrl1.webOpen("/doc/InsertSeal/PDF/AddSeal/test3.pdf");
        map.put("pageoffice", pdfCtrl1.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/PDF/AddSeal/PDF3");
        return mv;
    }

    @RequestMapping(value = "/DeleteSeal/PDF")
    public ModelAndView deleteSeal(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl1 = new PDFCtrl(request);

        pdfCtrl1.webOpen("/doc/InsertSeal/PDF/DeleteSeal/test.pdf");
        map.put("pageoffice", pdfCtrl1.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/PDF/DeleteSeal/PDF");
        return mv;
    }

    @RequestMapping(value = "/AddSign/PDF1")
    public ModelAndView showWord11(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl1 = new PDFCtrl(request);

        pdfCtrl1.webOpen("/doc/InsertSeal/PDF/AddSign/test1.pdf");
        map.put("pageoffice", pdfCtrl1.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/PDF/AddSign/PDF1");
        return mv;
    }


    @RequestMapping("/AddSeal/save")
    public void save(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "InsertSeal/PDF/AddSeal/" + fs.getFileName());
        fs.close();
    }

    @RequestMapping("/AddSign/save")
    public void save2(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "InsertSeal/PDF/AddSign/" + fs.getFileName());
        fs.close();
    }

    @RequestMapping("/DeleteSeal/save")
    public void save3(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "InsertSeal/PDF/DeleteSeal/" + fs.getFileName());
        fs.close();
    }

}
