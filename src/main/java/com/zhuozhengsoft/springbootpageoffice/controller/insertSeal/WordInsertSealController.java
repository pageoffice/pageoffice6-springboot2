package com.zhuozhengsoft.springbootpageoffice.controller.insertSeal;

import com.zhuozhengsoft.pageoffice.*;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.Map;

@RestController
@RequestMapping(value = "/InsertSeal/Word")
public class WordInsertSealController {
    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";


/*添加印章*/
    @RequestMapping(value = "/AddSeal/Word1")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test1.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word1");
        return mv;
    }


    @RequestMapping(value = "/AddSeal/Word2")
    public ModelAndView showWord2(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test2.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word2");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word3")
    public ModelAndView showWord3(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test3.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word3");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word4")
    public ModelAndView showWord4(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test4.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word4");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word5")
    public ModelAndView showWord5(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test5.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word5");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word6")
    public ModelAndView showWord6(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test6.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word6");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word7")
    public ModelAndView showWord7(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test7.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word7");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word8")
    public ModelAndView showWord8(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test8.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word8");
        return mv;
    }

    @RequestMapping(value = "/AddSeal/Word9")
    public ModelAndView showWord9(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSeal/test9.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSeal/Word9");
        return mv;
    }


/*添加手写签名*/
    @RequestMapping(value = "/AddSign/Word1")
    public ModelAndView showWord11(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSign/test1.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSign/Word1");
        return mv;
    }


    @RequestMapping(value = "/AddSign/Word2")
    public ModelAndView showWord12(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSign/test2.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSign/Word2");
        return mv;
    }

    @RequestMapping(value = "/AddSign/Word3")
    public ModelAndView showWord13(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSign/test3.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSign/Word3");
        return mv;
    }

    @RequestMapping(value = "/AddSign/Word4")
    public ModelAndView showWord14(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSign/test4.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSign/Word4");
        return mv;
    }

    @RequestMapping(value = "/AddSign/Word5")
    public ModelAndView showWord15(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/AddSign/test5.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/AddSign/Word5");
        return mv;
    }



/*删除印章*/
    @RequestMapping(value = "/DeleteSeal/Word")
    public ModelAndView showWord10(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/InsertSeal/Word/DeleteSeal/test.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("InsertSeal/Word/DeleteSeal/Word");
        return mv;
    }

/*保存文件*/
    @RequestMapping("/AddSeal/save")
    public void save(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "InsertSeal/Word/AddSeal/" + fs.getFileName());
        fs.close();
    }

    @RequestMapping("/AddSign/save")
    public void save2(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "InsertSeal/Word/AddSign/" + fs.getFileName());
        fs.close();
    }

    @RequestMapping("/DeleteSeal/save")
    public void save1(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "InsertSeal/Word/DeleteSeal/" + fs.getFileName());
        fs.close();
    }

}
