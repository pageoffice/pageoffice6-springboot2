package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.SheetWriter;
import com.zhuozhengsoft.pageoffice.excel.WorkbookWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/ExcelInsertImage")
public class ExcelInsertImageController {

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WorkbookWriter workBook = new WorkbookWriter();
        SheetWriter sheet1 = workBook.openSheet("Sheet1");
        sheet1.openCell("A1").setValue("[image]/doc/ExcelInsertImage/image/logo.jpg[/image]");

        poCtrl.setWriter(workBook);//此行必须

        //打开excel文档
        poCtrl.webOpen("/doc/ExcelInsertImage/test.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("ExcelInsertImage/Excel");
        return mv;
    }

}
