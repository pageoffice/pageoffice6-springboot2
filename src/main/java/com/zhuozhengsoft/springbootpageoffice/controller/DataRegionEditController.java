package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.*;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.Map;

@RestController
@RequestMapping(value = "/DataRegionEdit")
public class DataRegionEditController {
    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    //编辑Word模板数据区域
    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WordDocumentWriter doc = new WordDocumentWriter();
        doc.getTemplate().defineDataRegion("PO_Guarantor", "[担保人]");
        doc.getTemplate().defineDataRegion("PO_SupplierAddress", "[供货单位地址]");
        doc.getTemplate().defineDataRegion("PO_BuyerAddress", "[购货单位地址]");
        doc.getTemplate().defineDataRegion("PO_No", "[合同编号]");
        doc.getTemplate().defineDataRegion("PO_GuarantorPhone", "[担保人手机]");
        doc.getTemplate().defineDataRegion("PO_ProductName", "[产品名称]");
        doc.getTemplate().defineDataRegion("PO_Buyer", "[购货单位]");
        doc.getTemplate().defineDataRegion("PO_Supplier", "[供货单位]");

        poCtrl.setWriter(doc);
        //打开Word文档
        poCtrl.webOpen("/doc/DataRegionEdit/test.docx", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DataRegionEdit/Word");
        return mv;
    }


    @RequestMapping("/save")
    public void save(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "DataRegionEdit/" + fs.getFileName());
        fs.close();
    }



}
