package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.ExcelCellWriter;
import com.zhuozhengsoft.pageoffice.excel.SheetWriter;
import com.zhuozhengsoft.pageoffice.excel.WorkbookWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

@RestController
@RequestMapping(value = "/ExcelFill")
public class ExcelFillController {

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //定义WorkbookWriter对象
        WorkbookWriter workBook = new WorkbookWriter();
        //定义SheetWriter对象，"Sheet1"是打开的Excel表单的名称
        SheetWriter sheet = workBook.openSheet("Sheet1");
        //定义ExcelCellWriter对象
        ExcelCellWriter cellB4 = sheet.openCell("B4");
        //给单元格赋值
        cellB4.setValue("1月");

        ExcelCellWriter cellC4 = sheet.openCell("C4");
        cellC4.setValue("300");

        ExcelCellWriter cellD4 = sheet.openCell("D4");
        cellD4.setValue("270");

        ExcelCellWriter cellE4 = sheet.openCell("E4");
        cellE4.setValue("270");

        ExcelCellWriter cellF4 = sheet.openCell("F4");
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
        cellF4.setValue(df.format(270.00 / 300 * 100) + "%");

        poCtrl.setWriter(workBook);

        //打开excel文档
        poCtrl.webOpen("/doc/ExcelFill/test.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("ExcelFill/Excel");
        return mv;
    }


}
