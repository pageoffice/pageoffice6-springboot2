# pageoffice6-springboot2

**当前版本：6.5.0.1**

### 一、简介

​	pageoffice6-springboot2项目演示了springboot框架和thymeleaf模板结合下如何使用PageOffice V6.0产品，此项目演示了PageOffice产品近90%的功能，是一个demo项目。

### 二、项目环境要求

​	Intelij IDEA,jdk1.8及以上版本

### 三、项目运行准备

​	1.在后端项目所在磁盘上新建一个pageoffice系统文件夹，例如：D:/pageoffice（此文件夹将用来存放PageOffice注册后生成的授权文件 “license.lic 或 license_gc.lic” 和 PageOffice客户端程序）。

​	2. 下载PageOffice客户端程序并拷贝到pageoffice系统文件夹下。

- 说明：

  PageOffice按客户端电脑操作系统不同分为两款产品。 **PageOffice Windows版** 和 **PageOffice国产版**。

  **PageOffice Windows版** ：支持Win7/Win8/Win10/Win11。

  **PageOffice** **国产版** ：支持信创系统，支持银河麒麟V10和统信UOS，支持X86（intel、兆芯等）、ARM（飞腾、鲲鹏等）、龙芯（Loogarch64）、龙芯（MIPS）芯片架构。

  如果您的用户客户端电脑都是Windows系统，只需要下载windows版客户端。

  如果您的用户客户端电脑都是国产系统，只需要下载对应cpu芯片的国产版客户端。

  如果您的用户客户端电脑既有Windows系统又有国产系统，那么Windows安装包和国产版安装包（根据cpu芯片选择）都需要下载。

  _注意：如果您需要支持WinXP，您只能使用PageOffice5.0及早期老版本。 如果您需要支持申威架构，您目前只能使用PageOffice国产版5.0_  。

- 下载地址：

  **注意**：下面地址随着时间推移可能会失效，请下载到本地做好备份。或者直接点击 [pageoffice6-client](https://gitee.com/pageoffice/pageoffice6-client/releases) 获取pageoffice最新版本。

   Windows版客户端下载地址：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/posetup_6.5.0.1.exe

   国产版客户端下载地址：
   
   **统信UOS系统：**

   X86芯片(Intel、兆芯)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_amd64_uos.deb

   ARM芯片(鲲鹏、飞腾)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_arm64_uos.deb
  
    **银河麒麟V10系统：**
    
    X86芯片(Intel、兆芯)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_amd64_kylin.deb
    
    ARM芯片(鲲鹏、飞腾)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_arm64_kylin.deb

3.如果用到PageOffice自带电子印章功能，还需要拷贝根目录下的 “poseal.db" 文件到上面创建的pageoffice系统文件夹下。
         
**注意：客户端程序的版本必须和服务器端pom文件里面依赖pageoffice的版本号6.X.X.X-javax保持一致。**

### 四、项目运行步骤
​	1.打开application.properties文件，将posyspath变量的值配置成您上一步新建的PageOffice系统文件夹 （例如：D:/pageoffice）。

​	2.运行项目：用IDEA打开项目，点击运行按钮即可。

​	3.打开浏览器，访问http://localhost:8081，点击页面中“最简单的打开保存文件”的超链接即可查看效果。

### 五、PageOffice序列号
在首次运行项目中点击 “在线打开文档” ，pageoffice 会弹出注册对话框。如果您需要使用 pageoffice windows 版，请在windows 客户端电脑上访问pageoffice页面，用windows 版序列号进行注册。如果您需要使用 pageoffice 国产版，请在国产操作系统的客户端电脑上访问pageoffice页面，用国产版序列号进行注册。

**PageOffice windows 版试用序列号：**

PageOfficeV6.0标准版试用序列号：A7VHK-HDTK-338U-NARCV

PageOfficeV6.0专业版试用序列号：6VD6L-3MJL-DASM-YD9B5

**PageOffice 国产版试用序列号：**

PageOfficeV6.0国产版试用序列号：GC-8H-693A-Z1IA-R62SJ



> 问：PageOffice浏览器窗口中如何调试？
>
> 答：PageOffice浏览器（POBrowser窗口）中，在网页空隙中右键"网页调试工具"，即可出现"PageOffice Devtools"工具窗口进行调试。

### 六、集成PageOffice到您的项目中的关键步骤
1. 在您项目的pom.xml中通过下面的代码引入PageOffice依赖。

   > pageoffice.jar的所有Releases版本都已上传到了Maven中央仓库，具体您要引用哪个版本请在Maven中央仓库地址中查看，建议使用Maven中央仓库中pageoffice.jar的最新版本。(Maven中央仓库中pageoffice.jar的地址：https://mvnrepository.com/artifact/com.zhuozhengsoft/pageoffice)

```xml
<dependency>
     <groupId>com.zhuozhengsoft</groupId>   
  <artifactId>pageoffice</artifactId>   
  <version>6.5.0.1-javax</version>
</dependency>
```

2. 在您项目的启动类Application类中配置如下代码。

```java
@Bean
    public ServletRegistrationBean pageofficeRegistrationBean()  {
        com.zhuozhengsoft.pageoffice.poserver.Server poserver = new com.zhuozhengsoft.pageoffice.poserver.Server();
        /**如果当前项目是打成jar或者war包运行，强烈建议将license的路径更换成某个固定的绝对路径下，不要放当前项目文件夹下,为了防止每次重新发布项目导致license丢失问题。
          * 比如windows服务器下：D:/pageoffice/，linux服务器下:/root/pageoffice/
         */
        //设置PageOffice注册成功后,license.lic文件存放的目录
        poserver.setSysPath(poSysPath);//poSysPath可以在application.properties这个文件中配置，也可以直设置文件夹路径，比如：poserver.setSysPath("D:/pageoffice");
        ServletRegistrationBean srb = new ServletRegistrationBean(poserver);
        srb.addUrlMappings("/poserver.zz");
        srb.addUrlMappings("/poclient");
        srb.addUrlMappings("/pageoffice.js");
        srb.addUrlMappings("/sealsetup.exe");
        return srb;
    }
```

  3.新建Controller并调用PageOffice，例如：

```java
public class PageOfficeController {
@RequestMapping(value = "/Word", method = RequestMethod.GET)
  public ModelAndView showWord(HttpServletRequest request) {
  PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
  poCtrl.webOpen("/doc/test.doc", OpenModeType.docNormalEdit, "张三");
  request.setAttribute("pageoffice", poCtrl.getHtml());
  ModelAndView mv = new ModelAndView("Excel.html");
   return mv;
  }
}
```

   4.新建View页面,例如：Word.html（PageOfficeCtroller返回的View页面，用来嵌入PageOffice控件)，PageOffice在View页面输出的代码如下：

```javascript
<div style="width: auto; height: 700px;" th:utext="${pageoffice}">
```

  5.在要打开文件的页面的head标签中先引用pageoffice.js文件后，再调POBrowser.openWindow()方法打开文件，例如：

```
<!--pageoffice.js的引用路径来自于第2步的项目启动类中的配置路径,一般将此js配置到了当前项目的根目录下 -->
<script type="text/javascript" src="/pageoffice.js"></script>

<!--openWindow()方法的第一个参数指向的url路径是指调用pageoffice打开文件的controller路径,比如下面的"SimpleWord/Word"-->
<a href="javascript:POBrowser.openWindow('SimpleWord/Word', 'width=1050px;height=900px;');">最简单在线打开保存Word文件(URL地址方式)</a>
```

### 七、电子印章功能说明

​     如果您的项目要用到PageOffice自带电子印章功能，请按下面的步骤进行操作。

​     1.请在您的web项目的启动类Application类中加上印章功能相关配置代码，具体代码请参考当前项目pageoffice6-springboot2启动类中电子印章功能配置代码，此处不再赘述。

> ​    注意：adminSeal.setSysPath(poSysPath)中的poSysPath指向的地址必须是您当前项目license.lic文件所在的目录。

​    2.请拷贝当前项目根目录下poseal.db文件到您的web项目的pageoffice系统文件夹下(例如：D:/pageoffice)。

​    3.请参考当前项目的[一、15、演示加盖印章和签字功能（以Word为例）](http://localhost:8081/InsertSeal/index)  示例代码进行盖章功能调用。


### 八、联系我们

卓正官网：[https://www.zhuozhengsoft.com](https://gitee.com/link?target=http%3A%2F%2Fwww.zhuozhengsoft.com)

PageOffice开发者中心：[https://www.pageoffice.cn/](https://gitee.com/link?target=https%3A%2F%2Fwww.pageoffice.cn%2F)

视频演示地址：

- Windows版：[https://www.bilibili.com/video/BV1M34y1A7qL](https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1M34y1A7qL)
- 国产版：[https://www.bilibili.com/video/BV1eM4m167Lc/](https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1eM4m167Lc%2F)

联系电话：400-6600-770

QQ: 800038353