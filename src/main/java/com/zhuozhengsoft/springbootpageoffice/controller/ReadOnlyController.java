package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/ReadOnly")
public class ReadOnlyController {

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
		WordDocumentWriter wordDoc=new WordDocumentWriter();
        wordDoc.setDisableWindowSelection(true);//禁止选中
        wordDoc.setDisableWindowRightClick(true);//禁止右键
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

		poCtrl.setWriter(wordDoc);//注意：使用WordDocumentWriter类后，此句必须
        poCtrl.setAllowCopy(false);//禁止复制
        //打开Word文档
        poCtrl.webOpen("/doc/ReadOnly/test.doc", OpenModeType.docReadOnly, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("ReadOnly/Word");
        return mv;
    }
}
