package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.*;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

@RestController
@RequestMapping(value = "/DefinedNameTable")
public class DefinedNameTableController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/index")
    public ModelAndView showindex() {

        ModelAndView mv = new ModelAndView("DefinedNameTable/index");
        return mv;
    }

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //定义Workbook对象
        WorkbookWriter workBook = new WorkbookWriter();
        //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
        SheetWriter sheet = workBook.openSheet("Sheet1");
        ExcelTableWriter table = sheet.openTableByDefinedName("report", 10, 5, false);
        table.getDataFields().get(0).setValue("轮胎");
        table.getDataFields().get(1).setValue("100");
        table.getDataFields().get(2).setValue("120");
        table.getDataFields().get(3).setValue("500");
        table.getDataFields().get(4).setValue("120%");
        table.nextRow();
        table.close();
        poCtrl.setWriter(workBook);

        //打开excel文档
        poCtrl.webOpen("/doc/DefinedNameTable/test.xls", OpenModeType.xlsSubmitForm, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DefinedNameTable/Excel");
        return mv;
    }

    @RequestMapping(value = "/Excel2")
    public ModelAndView showWord2(HttpServletRequest request, Map<String, Object> map) {
        String tempFileName = request.getParameter("temp");
        String temptext;
        if(tempFileName.equals("temp1.xls")){
            temptext = "模板一填充数据后显示的效果";
        }else {
            temptext = "模板二填充数据后显示的效果";
        }
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //定义Workbook对象
        WorkbookWriter workBook = new WorkbookWriter();
        //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
        SheetWriter sheet = workBook.openSheet("Sheet1");

        //定义Table对象，参数“report”就是Excel模板中定义的单元格区域的名称
        ExcelTableWriter table = sheet.openTableByDefinedName("report", 10, 5, true);
        //给区域中的单元格赋值
        table.getDataFields().get(0).setValue("轮胎");
        table.getDataFields().get(1).setValue("100");
        table.getDataFields().get(2).setValue("120");
        table.getDataFields().get(3).setValue("500");
        table.getDataFields().get(4).setValue("120%");
        //循环下一行
        table.nextRow();
        //关闭table对象
        table.close();
        //定义单元格对象，参数“year”就是Excel模板中定义的单元格的名称
        ExcelCellWriter cellYear = sheet.openCellByDefinedName("year");
        // 给单元格赋值
        Calendar c = new GregorianCalendar();
        int year = c.get(Calendar.YEAR);//获取年份
        cellYear.setValue(year + "年");
        ExcelCellWriter cellName = sheet.openCellByDefinedName("name");
        cellName.setValue("张三");
        poCtrl.setWriter(workBook);
        //打开excel文档
        poCtrl.webOpen("/doc/DefinedNameTable/" + tempFileName, OpenModeType.xlsSubmitForm, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        map.put("temptext", temptext);
        ModelAndView mv = new ModelAndView("DefinedNameTable/Excel2");
        return mv;
    }

    @RequestMapping(value = "/Excel6")
    public ModelAndView showWord6(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //打开excel文档
        poCtrl.webOpen("/doc/DefinedNameTable/test4.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DefinedNameTable/Excel6");
        return mv;
    }

    @RequestMapping(value = "/Excel4")
    public ModelAndView showWord4(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //定义Workbook对象
        WorkbookWriter workBook = new WorkbookWriter();
        //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
        SheetWriter sheet = workBook.openSheet("Sheet1");
        //定义Table对象
        ExcelTableWriter table = sheet.openTable("B4:F11");

        int rowCount = 12;//假设将要自动填充数据的实际记录条数为12
        for (int i = 1; i <= rowCount; i++) {
            //给区域中的单元格赋值
            table.getDataFields().get(0).setValue(i + "月");
            table.getDataFields().get(1).setValue("100");
            table.getDataFields().get(2).setValue("120");
            table.getDataFields().get(3).setValue("500");
            table.getDataFields().get(4).setValue("120%");
            table.nextRow();//循环下一行，此行必须
        }
        //关闭table对象
        table.close();
        //定义Table对象
        ExcelTableWriter table2 = sheet.openTable("B13:F16");
        int rowCount2 = 4;//假设将要自动填充数据的实际记录条数为12
        for (int i = 1; i <= rowCount2; i++) {
            //给区域中的单元格赋值
            table2.getDataFields().get(0).setValue(i + "季度");
            table2.getDataFields().get(1).setValue("300");
            table2.getDataFields().get(2).setValue("300");
            table2.getDataFields().get(3).setValue("300");
            table2.getDataFields().get(4).setValue("100%");
            table2.nextRow();
        }

        //关闭table对象
        table2.close();
        poCtrl.setWriter(workBook);
        //打开excel文档
        poCtrl.webOpen("/doc/DefinedNameTable/test4.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DefinedNameTable/Excel4");
        return mv;
    }

    @RequestMapping(value = "/Excel5")
    public ModelAndView showWord5(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //定义Workbook对象
        WorkbookWriter workBook = new WorkbookWriter();
        //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
        SheetWriter sheet = workBook.openSheet("Sheet1");

        //定义Table对象，参数“report1”为Excel中定义的名称，“4”为名称指定区域的行数，
        //“5”为名称指定区域的列数，“true”表示表格会按实际数据行数自动扩展
        ExcelTableWriter table = sheet.openTableByDefinedName("report", 4, 5, true);

        int rowCount = 12;//假设将要自动填充数据的实际记录条数为12
        for (int i = 1; i <= rowCount; i++) {
            //给区域中的单元格赋值
            table.getDataFields().get(0).setValue(i + "月");
            table.getDataFields().get(1).setValue("100");
            table.getDataFields().get(2).setValue("120");
            table.getDataFields().get(3).setValue("500");
            table.getDataFields().get(4).setValue("120%");
            table.nextRow();//循环下一行，此行必须
        }
        //关闭table对象
        table.close();
        //定义Table对象
        ExcelTableWriter table2 = sheet.openTableByDefinedName("report2", 4, 5, true);
        int rowCount2 = 4;//假设将要自动填充数据的实际记录条数为12
        for (int i = 1; i <= rowCount2; i++) {
            //给区域中的单元格赋值
            table2.getDataFields().get(0).setValue(i + "季度");
            table2.getDataFields().get(1).setValue("300");
            table2.getDataFields().get(2).setValue("300");
            table2.getDataFields().get(3).setValue("300");
            table2.getDataFields().get(4).setValue("100%");
            table2.nextRow();
        }
        //关闭table对象
        table2.close();
        poCtrl.setWriter(workBook);
        //打开excel文档
        poCtrl.webOpen("/doc/DefinedNameTable/test4.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DefinedNameTable/Excel5");
        return mv;
    }


    @RequestMapping("/save")
    public void save(HttpServletRequest request, HttpServletResponse response, Map<String, Object> map) throws IOException {
        response.setCharacterEncoding("utf-8");//解决返回的数据中文乱码问题
        WorkbookReader workBook = new WorkbookReader(request, response);
        SheetReader sheet = workBook.openSheet("Sheet1");
        ExcelTableReader table = sheet.openTableByDefinedName("report");
        String content = "";
        int result = 0;
        while (!table.getEOF()) {
            //获取提交的数值
            if (!table.getDataFields().getIsEmpty()) {
                content += "\r\n月份名称："
                        + table.getDataFields().get(0).getText();
                content += "\r\n计划完成量："
                        + table.getDataFields().get(1).getText();
                content += "\r\n实际完成量："
                        + table.getDataFields().get(2).getText();
                content += "\r\n累计完成量："
                        + table.getDataFields().get(3).getText();
                //out.print(table.getDataFields().get(2).getText()+"      mmmmmmmmmmmmm          "+table.getDataFields().get(1).getText());

                int colCount = table.getDataFields().size();//获取列数

                if (table.getDataFields().get(2).getText().equals(null)
                        || table.getDataFields().get(2).getText().trim().length() == 0) {
                    content += "\r\n完成率：0%";
                } else {
                    float f = Float.parseFloat(table.getDataFields().get(2)
                            .getText());
                    f = f / Float.parseFloat(table.getDataFields().get(1).getText());
                    DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
                    content += "\r\n完成率：" + df.format(f * 100) + "%";
                }
                content += "\r\n*********************************************";
            }
            //循环进入下一行
            table.nextRow();
        }
        table.close();
        /**
         * 实际开发中，一般获取数据区域的值后用来和数据库进行交互，比如根据刚才获取的数据进行数据库记录的新增，更新，删除等。
         * 此处为了给用户展示获取的数据内容，通过setCustomSaveResult将获取的数据区域的值返回到前端页面给用户检查执行的结果。
         * 如果只是想返回一个保存结果，可以使用比如：setCustomSaveResult("ok")，前端可以根据这个保存结果进行下一步逻辑处理。
         */
        workBook.setCustomSaveResult(content);
        workBook.close();
    }
}
