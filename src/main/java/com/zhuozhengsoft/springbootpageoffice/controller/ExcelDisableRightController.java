package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.WorkbookWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import java.util.Map;

@RestController
@RequestMapping(value = "/ExcelDisableRight")
public class ExcelDisableRightController {

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
          
        WorkbookWriter workBoook = new WorkbookWriter();
        workBoook.setDisableSheetRightClick(true);//禁止当前工作表鼠标右键
        poCtrl.setWriter(workBoook);

        //打开excel文档
        poCtrl.webOpen("/doc/ExcelDisableRight/test.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("ExcelDisableRight/Excel");
        return mv;
    }


}
