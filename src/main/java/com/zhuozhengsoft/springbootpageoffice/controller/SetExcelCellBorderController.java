package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.ExcelBorder;
import com.zhuozhengsoft.pageoffice.excel.ExcelTableWriter;
import com.zhuozhengsoft.pageoffice.excel.SheetWriter;
import com.zhuozhengsoft.pageoffice.excel.WorkbookWriter;
import com.zhuozhengsoft.pageoffice.excel.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.util.Map;

@RestController
@RequestMapping(value = "/SetExcelCellBorder")
public class SetExcelCellBorderController {

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WorkbookWriter wb = new WorkbookWriter();
        SheetWriter sheet = wb.openSheet("Sheet1");
        // 设置背景
        ExcelTableWriter backGroundTable = sheet.openTable("A1:P200");
        //设置表格边框样式
        backGroundTable.getBorder().setLineColor(Color.white);

        // 设置单元格边框样式
        ExcelBorder C4Border = sheet.openTable("C4:C4").getBorder();
        C4Border.setWeight(XlBorderWeight.xlThick);
        C4Border.setLineColor(Color.yellow);
        C4Border.setBorderType(XlBorderType.xlAllEdges);

        // 设置单元格边框样式
        ExcelBorder B6Border = sheet.openTable("B6:B6").getBorder();
        B6Border.setWeight(XlBorderWeight.xlHairline);
        B6Border.setLineColor(Color.magenta);
        B6Border.setLineStyle(XlBorderLineStyle.xlSlantDashDot);
        B6Border.setBorderType(XlBorderType.xlAllEdges);

        //设置表格边框样式
        ExcelTableWriter titleTable = sheet.openTable("B4:F5");
        titleTable.getBorder().setWeight(XlBorderWeight.xlThick);
        titleTable.getBorder().setLineColor(new Color(0, 128, 128));
        titleTable.getBorder().setBorderType(XlBorderType.xlAllEdges);

        //设置表格边框样式
        ExcelTableWriter bodyTable2 = sheet.openTable("B6:F15");
        bodyTable2.getBorder().setWeight(XlBorderWeight.xlThick);
        bodyTable2.getBorder().setLineColor(new Color(0, 128, 128));
        bodyTable2.getBorder().setBorderType(XlBorderType.xlAllEdges);

        poCtrl.setWriter(wb);

        //打开excel文档
        poCtrl.webOpen("/doc/SetExcelCellBorder/test.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("SetExcelCellBorder/Excel");
        return mv;
    }


}
