package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/OpenWord")
public class OpenWordController {

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/OpenWord/test.doc", OpenModeType.docReadOnly, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("OpenWord/Word");
        return mv;
    }


}
