package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.DocumentOpenType;
import com.zhuozhengsoft.pageoffice.FileMakerCtrl;
import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping(value = "/FileMakerModify")
public class FileMakerModifyController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/Default")
    public ModelAndView index(HttpServletRequest request) {

        ModelAndView mv = new ModelAndView("FileMakerModify/Default");
        return mv;
    }


    @RequestMapping(value = "/FileMakerModify")
    public void showWord(HttpServletRequest request,HttpServletResponse response) throws IOException {
        FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
        WordDocumentWriter doc = new WordDocumentWriter();
        //给数据区域赋值，即把数据填充到模板中相应的位置
        doc.openDataRegion("PO_Date").setValue(new SimpleDateFormat("yyyy年MM月dd日").format(new Date()).toString());

        fmCtrl.setWriter(doc);
        fmCtrl.fillDocument("/doc/FileMakerModify/maker.doc", DocumentOpenType.Word);
        response.getOutputStream().print(fmCtrl.getHtml());
    }

    @RequestMapping(value = "/save")
    public void save(HttpServletRequest request,HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        String fileName = "maker" + fs.getFileExtName();
        fs.saveToFile(dir + "FileMakerModify/" + fileName);
        //设置自定义保存结果，用来返回给前端页面，setCustomSaveResult的参数也可以是json字符串类型
        //设置文件的保存结果
        fs.setCustomSaveResult("ok");
        fs.close();
    }
}
