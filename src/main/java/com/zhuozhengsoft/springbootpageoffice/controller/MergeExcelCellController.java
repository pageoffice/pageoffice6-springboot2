package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.excel.ExcelCellWriter;
import com.zhuozhengsoft.pageoffice.excel.SheetWriter;
import com.zhuozhengsoft.pageoffice.excel.WorkbookWriter;
import com.zhuozhengsoft.pageoffice.excel.*;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.util.Map;

@RestController
@RequestMapping(value = "/MergeExcelCell")
public class MergeExcelCellController {

    @RequestMapping(value = "/Excel")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WorkbookWriter wb = new WorkbookWriter();
        SheetWriter sheet = wb.openSheet("Sheet1");
        //合并单元格
        sheet.openTable("B2:F2").merge();
        ExcelCellWriter cB2 = sheet.openCell("B2");
        cB2.setValue("北京某公司产品销售情况");
        //设置水平对齐方式
        cB2.setHorizontalAlignment(XlHAlign.xlHAlignCenter);
        cB2.setForeColor(Color.red);
        cB2.getFont().setSize(16);

        sheet.openTable("B4:B6").merge();
        ExcelCellWriter cB4 = sheet.openCell("B4");
        cB4.setValue("A产品");
        //设置水平对齐方式
        cB4.setHorizontalAlignment(XlHAlign.xlHAlignCenter);
        //设置垂直对齐方式
        cB4.setVerticalAlignment(XlVAlign.xlVAlignCenter);

        sheet.openTable("B7:B9").merge();
        ExcelCellWriter cB7 = sheet.openCell("B7");
        cB7.setValue("B产品");
        cB7.setHorizontalAlignment(XlHAlign.xlHAlignCenter);
        cB7.setVerticalAlignment(XlVAlign.xlVAlignCenter);

        poCtrl.setWriter(wb);

        //打开excel文档
        poCtrl.webOpen("/doc/MergeExcelCell/test.xls", OpenModeType.xlsNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("MergeExcelCell/Excel");
        return mv;
    }

}
