package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.DataRegionWriter;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.FileNotFoundException;
import java.util.Map;

@RestController
@RequestMapping(value = "/DataRegionFill2")
public class DataRegionFill2Controller {

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WordDocumentWriter doc = new WordDocumentWriter();
        //打开数据区域
        DataRegionWriter dataRegion1 = doc.openDataRegion("PO_userName");
        //给数据区域赋值
        dataRegion1.setValue("张三");
        //设置字体样式
        dataRegion1.getFont().setColor(Color.blue);
        dataRegion1.getFont().setSize(24);
        dataRegion1.getFont().setName("隶书");
        dataRegion1.getFont().setBold(true);

        DataRegionWriter dataRegion2 = doc.openDataRegion("PO_deptName");
        dataRegion2.setValue("销售部");
        dataRegion2.getFont().setColor(Color.red);

        poCtrl.setWriter(doc);
        //打开Word文档
        poCtrl.webOpen("/doc/DataRegionFill2/test.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DataRegionFill2/Word");
        return mv;
    }


}
