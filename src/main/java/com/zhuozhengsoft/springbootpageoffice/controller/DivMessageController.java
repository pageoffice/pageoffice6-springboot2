package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/DivMessage")
public class DivMessageController {
    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //打开Word文档
        poCtrl.webOpen("/doc/DivMessage/test.doc", OpenModeType.docNormalEdit, "张佚名");
        request.setAttribute("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DivMessage/Word");
        return mv;
    }


}
