package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.DataRegionWriter;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

@RestController
@RequestMapping(value = "/TaoHong")
public class TaoHongController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/index")
    public ModelAndView showindex() {
        ModelAndView mv = new ModelAndView("TaoHong/index");
        return mv;
    }

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/TaoHong/test.doc", OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("TaoHong/Word");
        return mv;
    }

    @RequestMapping(value = "/taoHong")
    public ModelAndView showtaoHong(HttpServletRequest request, Map<String, Object> map) {
        String fileName = "";
        String mbName = request.getParameter("mb");
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        if (mbName != null && mbName.trim() != "") {
            // 选择模板后执行套红
            fileName = request.getParameter("mb");

            // 填充数据和正文内容到“zhengshi.doc”
            WordDocumentWriter doc = new WordDocumentWriter();
            DataRegionWriter copies = doc.openDataRegion("PO_Copies");
            copies.setValue("6");
            DataRegionWriter docNum = doc.openDataRegion("PO_DocNum");
            docNum.setValue("001");
            DataRegionWriter issueDate = doc.openDataRegion("PO_IssueDate");
            issueDate.setValue("2013-5-30");
            DataRegionWriter issueDept = doc.openDataRegion("PO_IssueDept");
            issueDept.setValue("开发部");
            DataRegionWriter sTextS = doc.openDataRegion("PO_STextS");
            sTextS.setValue("[word]/doc/TaoHong/test.doc[/word]");
            DataRegionWriter sTitle = doc.openDataRegion("PO_sTitle");
            sTitle.setValue("北京某公司文件");
            DataRegionWriter topicWords = doc.openDataRegion("PO_TopicWords");
            topicWords.setValue("Pageoffice、 套红");
            poCtrl.setWriter(doc);
            //定义套红后生成的文件名
            String filename1 = "zhengshi.doc";
            poCtrl.setSaveFilePage("save?fileName="+filename1);//设置处理文件保存的请求方法
        } else {
            //首次加载时，加载正文内容：test.doc
            fileName = "test.doc";
            poCtrl.setSaveFilePage("save?fileName="+fileName);//设置处理文件保存的请求方法
        }

        //打开Word文档
        poCtrl.webOpen("/doc/TaoHong/" + fileName, OpenModeType.docNormalEdit, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("TaoHong/taoHong");
        return mv;
    }


    @RequestMapping(value = "/readOnly")
    public ModelAndView showreadOnly(HttpServletRequest request, Map<String, Object> map) {
        String fileName = "zhengshi.doc"; //正式发文的文件
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        //打开Word文档
        poCtrl.webOpen("/doc/TaoHong/" + fileName, OpenModeType.docReadOnly, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("TaoHong/readOnly");
        return mv;
    }

    @RequestMapping("/save")
    public void save(HttpServletRequest request, HttpServletResponse response) {
        String fileName = request.getParameter("fileName");
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "TaoHong/" + fileName);
        fs.close();
    }

}
