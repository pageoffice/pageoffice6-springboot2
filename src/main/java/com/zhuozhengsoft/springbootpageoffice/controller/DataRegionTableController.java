package com.zhuozhengsoft.springbootpageoffice.controller;


import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.*;

import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.Map;

import static java.lang.System.out;

@RestController
@RequestMapping(value = "/DataRegionTable")
public class DataRegionTableController {
    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        WordDocumentWriter doc = new WordDocumentWriter();
        //打开数据区域
        DataRegionWriter dTable = doc.openDataRegion("PO_table");
        //设置数据区域可编辑性
        dTable.setEditing(true);

        //打开数据区域中的表格，OpenTable(index)方法中的index为word文档中表格的下标，从1开始
        WordTableWriter table1 = doc.openDataRegion("PO_Table").openTable(1);
        //设置表格边框样式
        table1.getBorder().setLineColor(Color.green);
        table1.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
        // 设置表头单元格文本居中
        table1.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
        table1.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
        table1.openCellRC(2, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
        table1.openCellRC(3, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);

        // 给表头单元格赋值
        table1.openCellRC(1, 2).setValue("产品1");
        table1.openCellRC(1, 3).setValue("产品2");
        table1.openCellRC(2, 1).setValue("A部门");
        table1.openCellRC(3, 1).setValue("B部门");

        poCtrl.setWriter(doc);
        //打开Word文档
        poCtrl.webOpen("/doc/DataRegionTable/test.doc", OpenModeType.docSubmitForm, "张三");
        map.put("pageoffice", poCtrl.getHtml());
        ModelAndView mv = new ModelAndView("DataRegionTable/Word");
        return mv;
    }

    @RequestMapping("/save")
    public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("utf-8");//解决返回的数据中文乱码问题
        WordDocumentReader doc = new WordDocumentReader(request, response);
        DataRegionReader dataReg = doc.openDataRegion("PO_table");
        WordTableReader table = dataReg.openTable(1);

        /**
         * 实际开发中，一般获取数据区域的值后用来和数据库进行交互，比如根据刚才获取的数据进行数据库记录的新增，更新，删除等。
         * 此处为了给用户展示获取的数据内容，通过setCustomSaveResult将获取的数据区域的值返回到前端页面给用户检查执行的结果。
         * 如果只是想返回一个保存结果，可以使用比如：setCustomSaveResult("ok")，前端可以根据这个保存结果进行下一步逻辑处理。
         */
        for (int i = 1; i <= table.getRowsCount(); i++) {
            for (int j = 1; j <= table.getColumnsCount(); j++) {
                System.out.println("单元格("+i+","+j+")的值为:"+table.openCellRC(i, j).getValue());
            }

        }
        doc.setCustomSaveResult("ok");//通过setCustomSaveResult给前端页面返回数据
        doc.close();
    }

}
