package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.*;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping(value = "/FileMaker")
public class FileMakerController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/Default")
    public ModelAndView index(HttpServletRequest request) {

        ModelAndView mv = new ModelAndView("FileMaker/Default");
        return mv;
    }


    @RequestMapping(value = "/FileMaker")
    public void showWord(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String[] companyArr = {"空", "微软（中国）有限公司", "IBM（中国）服务有限公司", "亚马逊贸易有限公司", "脸书科技有限公司", "谷歌网络有限公司", "英伟达技术有限公司", "台积电科技有限责任公司", "沃尔玛股份有限公司"};
        int id = Integer.parseInt(request.getParameter("id"));
        FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
        WordDocumentWriter doc = new WordDocumentWriter();
        //给数据区域赋值，即把数据填充到模板中相应的位置
        doc.openDataRegion("PO_company").setValue(companyArr[id]);

        fmCtrl.setWriter(doc);
        fmCtrl.fillDocument("/doc/FileMaker/template.doc", DocumentOpenType.Word);
        response.getOutputStream().print(fmCtrl.getHtml());
    }

    @RequestMapping(value = "/save")
    public void save(HttpServletRequest request,HttpServletResponse response) {
        String id = request.getParameter("id");
        FileSaver fs = new FileSaver(request, response);
        String fileName = "maker" + id + fs.getFileExtName();
        fs.saveToFile(dir + "FileMaker/" + fileName);
        //设置自定义保存结果，用来返回给前端页面，setCustomSaveResult的参数也可以是json字符串类型
        //设置文件的保存结果
        fs.setCustomSaveResult("ok");
        fs.close();
    }
}
