package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.PDFCtrl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/POPDF")
public class POPDFController {

    @RequestMapping(value = "/PDF")
    public ModelAndView showPdf(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl = new PDFCtrl(request);
        pdfCtrl.webOpen("/doc/POPDF/test.pdf");

        map.put("pageoffice", pdfCtrl.getHtml());
        ModelAndView mv = new ModelAndView("POPDF/pdf");
        return mv;
    }


}
