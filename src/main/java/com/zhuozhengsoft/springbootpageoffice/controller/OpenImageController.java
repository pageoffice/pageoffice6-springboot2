package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.PDFCtrl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping(value = "/OpenImage")
public class OpenImageController {
    @RequestMapping(value = "/Image")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PDFCtrl pdfCtrl = new PDFCtrl(request);

        pdfCtrl.webOpen("/doc/OpenImage/test.jpg");

        map.put("pageoffice", pdfCtrl.getHtml());
        ModelAndView mv = new ModelAndView("OpenImage/Image");
        return mv;
    }

}
