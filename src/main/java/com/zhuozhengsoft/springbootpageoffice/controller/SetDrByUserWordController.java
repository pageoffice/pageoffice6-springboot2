package com.zhuozhengsoft.springbootpageoffice.controller;

import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import com.zhuozhengsoft.pageoffice.word.DataRegionWriter;
import com.zhuozhengsoft.pageoffice.word.WordDocumentWriter;
import com.zhuozhengsoft.springbootpageoffice.util.GetDirPathUtil;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping(value = "/SetDrByUserWord")
public class SetDrByUserWordController {

    //获取doc目录的磁盘路径
    private String dir = GetDirPathUtil.getDirPath() + "static/doc/";

    @RequestMapping(value = "/index")
    public ModelAndView showindex() {
        ModelAndView mv = new ModelAndView("SetDrByUserWord/index");
        return mv;
    }

    @RequestMapping(value = "/Word")
    public ModelAndView showWord(HttpServletRequest request, Map<String, Object> map) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

        String userName = request.getParameter("userName");
        //***************************卓正PageOffice组件的使用********************************
        WordDocumentWriter doc = new WordDocumentWriter();
        //打开数据区域
        DataRegionWriter dTitle = doc.openDataRegion("PO_title");
        //给数据区域赋值
        dTitle.setValue("某公司第二季度产量报表");
        //设置数据区域可编辑性
        dTitle.setEditing(false);//数据区域不可编辑

        DataRegionWriter dA1 = doc.openDataRegion("PO_A_pro1");
        DataRegionWriter dA2 = doc.openDataRegion("PO_A_pro2");
        DataRegionWriter dB1 = doc.openDataRegion("PO_B_pro1");
        DataRegionWriter dB2 = doc.openDataRegion("PO_B_pro2");

        //根据登录用户名设置数据区域可编辑性
        //A部门经理登录后
        if (userName.equals("zhangsan")) {
            userName = "A部门经理";
            dA1.setEditing(true);
            dA2.setEditing(true);
            dB1.setEditing(false);
            dB2.setEditing(false);
        }
        //B部门经理登录后
        else {
            userName = "B部门经理";
            dB1.setEditing(true);
            dB2.setEditing(true);
            dA1.setEditing(false);
            dA2.setEditing(false);
        }

        poCtrl.setWriter(doc);

        //打开Word文档
        poCtrl.webOpen("/doc/SetDrByUserWord/test.doc", OpenModeType.docSubmitForm, userName);
        map.put("pageoffice", poCtrl.getHtml());
        map.put("userName", userName);
        ModelAndView mv = new ModelAndView("SetDrByUserWord/Word");
        return mv;
    }


    @RequestMapping("/save")
    public void save(HttpServletRequest request, HttpServletResponse response) {
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile(dir + "SetDrByUserWord/" + fs.getFileName());
        fs.close();
    }

}
